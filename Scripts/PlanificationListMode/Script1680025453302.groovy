import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('/Users/gianellavezzoni/Downloads/app-debug.apk', true)

Mobile.tap(findTestObject('Object Repository/android.view.ViewGroup (12)'), 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/android.widget.TextView - Planification'), 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/android.widget.EditText - What are you looking at'), 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/android.view.ViewGroup (13)'), 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/android.view.ViewGroup (14)'), 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/android.view.ViewGroup (15)'), 0)

Mobile.tap(findTestObject(''), 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/android.widget.TextView - Task 0007'), 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/android.widget.TextView - test'), 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/android.widget.TextView - test (1)'), 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/android.view.ViewGroup (16)'), 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/android.widget.TextView - Pending'), 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/android.widget.TextView - Legal matrix'), 0)

Mobile.tap(findTestObject(''), 0)

Mobile.tap(findTestObject(''), 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/android.widget.TextView - Event 0025'), 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/android.widget.TextView - test (2)'), 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/android.widget.TextView - test (3)'), 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/android.view.ViewGroup (17)'), 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/android.widget.TextView - Pending actions'), 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/android.widget.TextView - Acto inseguro'), 0)

Mobile.tap(findTestObject(''), 0)

Mobile.tap(findTestObject(''), 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/android.widget.TextView - Action 0014'), 0)

Mobile.verifyElementVisible(findTestObject(''), 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/android.widget.TextView - accion'), 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/android.widget.TextView - Finished'), 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/android.widget.TextView - Prioridad Low'), 0)

Mobile.tap(findTestObject(''), 0)

Mobile.sendKeys(findTestObject(''), 'test')

Mobile.closeApplication()

